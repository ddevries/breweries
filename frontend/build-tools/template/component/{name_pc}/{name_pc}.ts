import Vue from 'vue';
import VueTypes from 'vue-types';
{{#if components}}
{{#each components}}
import {{this}} from '../{{this}}';
{{/each}}
{{/if}}
{{#if props}}

{{/if}}
// @vue/component
export default Vue.extend({
  name: '{{name_pc}}',
  {{#if components}}
  components: {
    {{#each components}}
    {{this}},
    {{/each}}
  },
	{{/if}}
  {{#if props}}
  props: {
    {{#each props}}
    {{this}}: VueTypes.any,
    {{/each}}
  },
  {{/if}}
  {{#if lifecycle}}
  beforeCreate() {},
  created() {},
  beforeMount() {},
  mounted() {},
  beforeUpdate() {},
  updated() {},
  beforeDestroy() {},
  destroyed() {},
  {{/if}}
});
