// @vue/component
import Vue from 'vue';
import { mapActions, mapGetters } from 'vuex';
import ModuleNames from '../../store/moduleNames';
import {
  GET_BREWERIES,
  GET_COUNTRIES,
  LOAD_BREWERIES,
} from '../../store/module/brewerydb/brewerydb';
import filterGroups from '../../data/filterGroups';
import BreweryTypes from '../../data/BreweryTypes';
import FilterItem from '../../component/FilterItem';
import BreweryList from '../../component/BreweryList/BreweryList';
import Header from '../../component/Header';
import Search from '../../component/Search';

export default Vue.extend({
  name: 'HomePage',
  components: {
    Header,
    Search,
    FilterItem,
    BreweryList,
  },
  mounted() {
    // @ts-ignore
    this.loadBreweries();
  },
  data() {
    return {
      filterGroupsTypes: filterGroups,
      group: this.$route.query.group || filterGroups.COUNTRY,
    };
  },
  watch: {
    group(newValue, oldValue) {
      if (oldValue === newValue) {
        return;
      }

      this.$router.push({
        query: {
          ...this.$route.query,
          group: newValue,
        },
      });
    },
  },
  computed: {
    ...mapGetters(ModuleNames.BREWERY_DB, {
      getBreweries: GET_BREWERIES,
      countries: GET_COUNTRIES,
    }),
    filterGroups(): any {
      // @ts-ignore
      const filters: any = [
        {
          id: filterGroups.TYPE,
          // @ts-ignore
          name: this.$t(`filter.${filterGroups.TYPE}`),
          filters: [
            {
              id: BreweryTypes.ORGANIC,
              // @ts-ignore
              name: this.$t(`filter.${BreweryTypes.ORGANIC}`),
            },
            {
              id: BreweryTypes.NON_ORGANIC,
              // @ts-ignore
              name: this.$t(`filter.${BreweryTypes.NON_ORGANIC}`),
            },
          ],
        },
        {
          id: filterGroups.COUNTRY,
          // @ts-ignore
          name: this.$t(`filter.${filterGroups.COUNTRY}`),
          // @ts-ignore
          filters: this.countries.map((country: any) => ({
            id: country.id,
            name: country.displayName,
          })),
        },
      ];

      return filters;
    },
  },
  methods: {
    ...mapActions(ModuleNames.BREWERY_DB, {
      loadBreweries: LOAD_BREWERIES,
    }),
  },
});
