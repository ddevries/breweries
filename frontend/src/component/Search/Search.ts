// @vue/component
import Vue from 'vue';
import debounce from 'lodash/debounce';

export default Vue.extend({
  name: 'Search',
  methods: {
    handleKeyUp: debounce(function(event: KeyboardEvent) {
      // @ts-ignore
      this.$router.push({
        query: {
          s: (<HTMLInputElement>event.target).value,
        },
      });
    }, 500),
  },
});
