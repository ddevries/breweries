// @vue/component
import Vue from 'vue';

function asArray(object: Array<string> | string | undefined) {
  if (!object) {
    return [];
  }

  return Array.isArray(object) ? [...object] : [object];
}

function addOrRemoveFilterFromQuery(filter: string, itemToAdd: string, query: any) {
  const filterAsArray = asArray(query[filter]);

  if (!filterAsArray.includes(itemToAdd)) {
    filterAsArray.push(itemToAdd);
  } else {
    filterAsArray.splice(query[filter].indexOf(itemToAdd), 1);
  }

  return {
    ...query,
    [filter]: filterAsArray,
  };
}

export default Vue.extend({
  name: 'FilterItem',
  props: {
    // eslint-disable-next-line vue-types/require-default-prop
    filterGroup: Object,
    filter: Object,
  },
  methods: {
    isActive(filterGroup: any, filter: any) {
      return asArray(this.$route.query[filterGroup.id]).includes(filter.id);
    },
    handleFilterChange(filterGroup: any, filter: any) {
      const query = addOrRemoveFilterFromQuery(
        filterGroup.id,
        String(filter.id),
        // @ts-ignore
        Object.assign({}, this.$route.query),
      );

      // @ts-ignore
      this.$router.push({
        query,
      });
    },
  },
});
