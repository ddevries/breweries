// @vue/component
import Vue from 'vue';

export default Vue.extend({
  name: 'BreweryList',
  props: {
    // eslint-disable-next-line vue-types/require-default-prop
    breweryGroups: Array,
  },
  methods: {},
});
