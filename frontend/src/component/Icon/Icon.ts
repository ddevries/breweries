import Vue from 'vue';
import VueTypes from 'vue-types';

const svgContext = require.context('../../asset/svg/?inline', false, /\.svg/);

export default Vue.extend({
  name: 'Icon',
  props: {
    name: VueTypes.string.isRequired,
  },
  computed: {
    icon(): any {
      return svgContext(`./${this.name}.svg`);
    },
  },
});
