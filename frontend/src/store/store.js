import Vuex from 'vuex';
import Vue from 'vue';
import moduleNames from './moduleNames';

import app from './module/app';
import brewerydb from './module/brewerydb';

Vue.use(Vuex);

let store = null;

const getStore = () => {
  if (!store) {
    store = new Vuex.Store({
      modules: {
        [moduleNames.APP]: app,
        [moduleNames.BREWERY_DB]: brewerydb,
      },
      strict: process.env.NODE_ENV !== 'production',
    });
  }

  return store;
};

export default getStore;
