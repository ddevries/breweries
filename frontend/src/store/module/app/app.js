export const SET_DEVICE_STATE = 'setDeviceState';

export default {
  namespaced: true,
  state: {
    deviceState: null,
  },
  getters: {},
  mutations: {
    [SET_DEVICE_STATE](state, deviceState) {
      state.deviceState = deviceState;
    },
  },
  actions: {},
};
