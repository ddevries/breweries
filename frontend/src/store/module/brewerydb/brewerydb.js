import { GATEWAY } from '../../../data/Injectables';
import { getValue } from '../../../util/injector';
import BreweryTypes from '../../../data/BreweryTypes';
import filterGroups from '../../../data/filterGroups';

export const LOAD_BREWERIES = 'loadBreweries';
export const SET_BREWERIES = 'setBreweries';
export const GET_BREWERIES = 'getBreweries';
export const GET_COUNTRIES = 'getCountries';

const BREWERIES_ENDPOINT = 'breweries';

function reduceByCountry(accumulator, brewery) {
  const breweryLocation = brewery.locations.find(location => location.isPrimary === 'Y');
  const countryInAccumulator = accumulator.find(
    country => country.id === breweryLocation.country.isoCode,
  );

  if (!countryInAccumulator) {
    accumulator.push({
      id: breweryLocation.country.isoCode,
      displayName: breweryLocation.country.displayName,
      breweries: [brewery],
    });
  } else {
    countryInAccumulator.breweries.push(brewery);
  }

  return accumulator;
}

function reduceByType(accumulator, brewery) {
  const breweryType = brewery.isOrganic === 'Y' ? BreweryTypes.ORGANIC : BreweryTypes.NON_ORGANIC;
  const typeInAccumulator = accumulator.find(type => type.id === breweryType);

  if (!typeInAccumulator) {
    accumulator.push({
      id: breweryType,
      displayName: `group.${breweryType}`,
      breweries: [brewery],
    });
  } else {
    typeInAccumulator.breweries.push(brewery);
  }

  return accumulator;
}

function isQueriedType(type, brewery) {
  return (
    (brewery.isOrganic === 'Y' && type.includes(BreweryTypes.ORGANIC)) ||
    (brewery.isOrganic === 'N' && type.includes(BreweryTypes.NON_ORGANIC))
  );
}

function isQueriedCountry(country, brewery) {
  const breweryLocation = brewery.locations.find(location => location.isPrimary === 'Y');

  return country.includes(breweryLocation.country.isoCode);
}

function isQueried(brewery, query) {
  return (
    brewery.status === 'verified' &&
    (!query.s || brewery.name.toLowerCase().includes(query.s.toLowerCase())) &&
    (!query.type || query.type.length === 0 || isQueriedType(query.type, brewery)) &&
    (!query.country || query.country.length === 0 || isQueriedCountry(query.country, brewery))
  );
}

export default {
  namespaced: true,
  state: {
    breweries: [],
  },
  getters: {
    [GET_BREWERIES]: state => query =>
      state.breweries
        .filter(brewery => isQueried(brewery, query))
        .reduce(
          !query.group || query.group === filterGroups.COUNTRY ? reduceByCountry : reduceByType,
          [],
        ),
    [GET_COUNTRIES]: state =>
      state.breweries
        .filter(brewery => brewery.status === 'verified')
        .reduce((accumulator, brewery) => {
          const breweryLocation = brewery.locations.find(location => location.isPrimary === 'Y');
          const countryInAccumulator = accumulator.find(
            country => country.id === breweryLocation.country.isoCode,
          );

          if (!countryInAccumulator) {
            accumulator.push({
              id: breweryLocation.country.isoCode,
              displayName: breweryLocation.country.displayName,
            });
          }

          return accumulator;
        }, []),
  },
  mutations: {
    [SET_BREWERIES]: (state, payload) => {
      state.breweries = payload;
    },
  },
  actions: {
    [LOAD_BREWERIES]: async context => {
      // load the first page of breweries
      const response = await getValue(GATEWAY).get(BREWERIES_ENDPOINT, {
        params: {
          withLocations: 'Y',
        },
      });

      // Load additional pages if present
      // This is required because breweryDB does not allow paginated results to be sorted by location
      if (response.numberOfPages > 1) {
        // request all pages at once an await them all for performance
        const additionalResponses = await Promise.all(
          Array(...Array(response.numberOfPages - 1)).map((value, key) => {
            return getValue(GATEWAY).get(BREWERIES_ENDPOINT, {
              params: {
                p: key + 2,
                withLocations: 'Y',
              },
            });
          }),
        );

        additionalResponses.forEach(additonalResponse => {
          if (Array.isArray(additonalResponse.data)) {
            response.data = response.data.concat(additonalResponse.data);
          }
        });
      }

      // commit all breweries to the store once everything is loaded
      // to prevent unnecessary rerenders of components
      context.commit(SET_BREWERIES, response.data);

      return response;
    },
  },
};
