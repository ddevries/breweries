# breweries

A Vue application that displays a list of breweries from the breweryDB API.

### Assignment

This is a small project made for the frontend [assignment](Client-Side-Programming-Assessment-PXLWidgets.pdf) for Pixel Widgets: 

##### Core points

The assignment states that it's optional to create a NodeJS backend that serves as a (caching) proxy server,
but when I started this project, I soon realised that this proxy is necessary, as BreweryDB specifically states that their API
is not meant the be called directly by clients. The developers key is meant to stay private and any misuse could result in the key
to become invalid. When making this project I focused on these points

* NodeJS Proxy server
* VueJS Frontend
* Architecture: Making sure that, especially the frontend will be an actual buildable and deployable package that could be ready for production 

Right now, I kept the frontend and the backend as to separately buildable and runnable projects as subfolders in the same repo.
If this was a real project, a choice would have to be made wether to package both the frontend and the backend as a single application, 
or to split them into separate repositories, or set it up as a single-repo with workspaces or Lerna. 
This choice is of course dependent on infrastructure, Project size, Team size, etc.

### How to install
Both the frontend as the backend are based on Yarn, with Node 10. 

To run the backend: 

* Navigate to the proxy directory 
* Run 'Yarn' 
* Run 'Yarn start' 

To run the frontend: 

* Navigate to the frontend directory 
* Run 'Yarn' 
* Run 'Yarn dev' 

After that you should be good to go.

### Results

##### Backend
Currently, the backend is just a simple proxy server. It passes through any request directly to the BreweryDB API and mixes in the developers key.
At the moment, I don't have enough experience with building a NodeJS backend to quickly implement a true backend, or caching. 
In the future, some serverside caching could be implemented. Also some of the mechanisms that are now done on the frontend, such as aggregating the data of multiple request could be mode to the backend to increase performance.

##### Frontend
I spent a great deal on making sure the frontend would be an actual production ready application. This is why the frontend is based on the
[Vue Skeleton](https://github.com/hjeti/vue-skeleton), but modified to fit the needs of this project. This Vue skeleton is very feature complete.
It contains Vue with Vuex, and features a fully customisable config to package compile-time settings for different environments. This config could also be swapped for a run-time setup with environment variables. The skeleton is also very well documented in the README.md and on GitHub repository and is MIT licenced.

I started off implementing the BreweryDB API with paginated sorting, but realised the API does not support any Filtering, Sorting or Grouping based on a brewery's primary location. So I was forced to refactor it and write a system that retrieves all the breweries on initial load. This is not an ideal setup. In the future this could and should be moved to the backend to increase performance and to also re-enable paginated results.

All the filters and grouping options are stored in the route as query parameters. I personally don't like it when sites opt for the easier and cheaper option to store this in local state, is this causes problems when navigating between list- and detail-views, or when a person reloads or shares the page.
The route is made available to the vue components by the vue-router-sync module.

All labels are stored in a language json file which is imported by the i18n vue module. This adds support for multi language, should this be required later. Also, this json file could be loaded externally to enable CMS manageable content.

##### Todo's
I did not yet add unit-tests. I did made a start with it in branch feature/unit-tests but it took a bit more time to implement the webpack and babel config.

Also, I did not had the time to write types for common objects in the application. This is why you would still see ts-ignore statements and explicit type any throughout the application.

Should this project grow, it's probably a good idea to subtract more individual components for reusability.

##### Time
I spent a bit more time than originally anticipated. All in all, I think I spent about 12 hours. This was mainly caused by setting up the backend, refactoring the API calls and making sure the filtering is done through router query params.
